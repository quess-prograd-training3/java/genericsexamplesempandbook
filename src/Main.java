import com.example.Company;
import com.example.Employee;

import java.util.ArrayList;

public class Main {
    public static void main(String[] args) {
        Company company = new Company();
        Employee employee1 = new Employee(1, 100000, "IT");
        Employee employee2 = new Employee(2, 120000, "HR");
        company.addEmployee(employee1);
        company.addEmployee(employee2);
        ArrayList<Employee> employees = company.getEmployees();
        for (Employee employee : employees) {
            System.out.println("Employee ID: " + employee.getEmpId());
            System.out.println("Salary: " + employee.getSalary());
            System.out.println("Department Name: " + employee.getDepartment());
        }
        company.removeEmployee(2);
        employees = company.getEmployees();
        for (Employee employee : employees) {
            System.out.println("Employee ID: " + employee.getEmpId());
            System.out.println("Salary: " + employee.getSalary());
            System.out.println("Department Name: " + employee.getDepartment());
        }
    }
}
