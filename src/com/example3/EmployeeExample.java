package com.example3;

public class EmployeeExample<T, T1> {
        private T1 id;
        T salary;
        /*void accept(){
          Scanner scanner=new Scanner(System.in);
            System.out.println("Enter id");
          id=scanner.nextInt();
            System.out.println("Enter Salary");
          //salary=scanner.nextFloat();
        }*/
        void display(){
            System.out.println(id+" "+salary);
        }

        public EmployeeExample(T salary,T1 id) {
            this.salary = salary;
            this.id=id;
        }

        public static void main(String[] args) {
            EmployeeExample<String,String> employee=new EmployeeExample<>("Thirty Thousand","1");
            // employee.accept();
            employee.display();
            EmployeeExample<Float,Integer> employee1=new EmployeeExample<>(30000.00f,1);
            //employee.accept();
            employee1.display();
        }

}
