package com.example;

import java.util.ArrayList;
import java.util.List;

public class Company {
    private ArrayList<Employee> employees;

    public Company() {
        employees = new ArrayList<>();
    }

    public void addEmployee(Employee employee) {
        employees.add(employee);
    }

    public void removeEmployee(int employeeId) {
        for (Employee employee : employees) {
            if (employee.getEmpId() == employeeId) {
                employees.remove(employee);
                break;
            }
        }
    }

    public ArrayList<Employee> getEmployees() {
        return employees;
    }
}
