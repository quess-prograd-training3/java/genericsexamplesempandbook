package com.com.example3;


public class Employee2<T,T1> {
        private T1 id;
        T salary;
        /*void accept(){
          Scanner scanner=new Scanner(System.in);
            System.out.println("Enter id");
          id=scanner.nextInt();
            System.out.println("Enter Salary");
          //salary=scanner.nextFloat();
        }*/
        void display(){
            System.out.println(id+" "+salary);
        }

        public Employee2(T salary,T1 id) {
            this.salary = salary;
            this.id=id;
        }

        public static void main(String[] args) {
            Employee2<String,String> employee=new Employee2<String, String>("Thirty Thousand","1");
            // employee.accept();
            employee.display();
            Employee2<Float,Integer> employee1=new Employee2<Float, Integer>(30000.00f,1);
            //employee.accept();
            employee1.display();
        }
    }

