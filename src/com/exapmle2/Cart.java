package com.exapmle2;

import java.util.ArrayList;
import java.util.List;

public class Cart<T> {
    private List<Book<T>> books;
    public Cart(){
        books=new ArrayList<>();
    }
    public void addBook(Book<T> book){
        books.add(book);
    }
    public double totalBooksCost(){
        double totalAmount=0;
        for (Book<T> book:books) {
            totalAmount+=book.getPrice();
        }
        return totalAmount;
    }
}
