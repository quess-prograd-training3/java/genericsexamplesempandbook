package com.exapmle2;

public class Book<T> {
    /* A Book Stall is having heap of books.The shopkepper wants to create the list of books .
    The list should have bookprn number and the Bookname.A User can add the book in cart .
    The cart amount should be calculated and shopkepper should also maintain stock of every book
     */
    private T bookPrnNumber;
    private String bookName;
    private double price;

    public Book(T bookPrnNumber, String bookName, double price) {
        this.bookPrnNumber = bookPrnNumber;
        this.bookName = bookName;
        this.price = price;
    }

    public T getBookPrnNumber() {

        return bookPrnNumber;
    }

    public String getBookName() {

        return bookName;
    }

    public double getPrice() {

        return price;
    }
}
