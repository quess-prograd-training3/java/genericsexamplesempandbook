package com.exapmle2;

import java.util.ArrayList;
import java.util.List;

public class BookStall<T> {
    private List<Book<T>> books;

    public BookStall() {
        books=new ArrayList<>();
    }
    public void addBook(Book<T> book){
        books.add(book);
    }
    public List<Book<T>> getBooks(){
        return books;
    }
}
