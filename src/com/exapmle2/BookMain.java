package com.exapmle2;

import java.util.ArrayList;
import java.util.List;

public class BookMain {
    public static void main(String[] args) {
        Book<Integer> book1=new Book<>(1,"Java",400);
        Book<Integer> book2=new Book<>(102,"DBMS", 350);
        Book<Integer> book3=new Book<>(234, "Devops", 700);
        BookStall<Integer> bookStall=new BookStall<>();
        bookStall.addBook(book1);
        bookStall.addBook(book2);
        bookStall.addBook(book3);

        List<Book<Integer>> books=bookStall.getBooks();
        for (Book<Integer> book : books) {
            System.out.println("Book PRN Number: " + book.getBookPrnNumber());
            System.out.println("Book Name: " + book.getBookName());
            System.out.println("Price: " + book.getPrice());
        }
        Cart<Integer> cart = new Cart<>();
        cart.addBook(book1);
        cart.addBook(book2);
        cart.addBook(book3);
        System.out.println("Total Amount: " + cart.totalBooksCost());
    }
}
